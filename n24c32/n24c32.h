/*
 * File:   n24c32.h
 * Author: Miroslav Soukup
 * Description: Header file of n24c32 eeprom memory driver.
 * 
 */



#ifndef N24C32_H // Protection against multiple inclusion
#define N24C32_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define N24C32_I2C_CLIENT_ADDRESS   UINT8_C(0b1010000)

#define N24C32_WORDS_COUNT_MAX      UINT32_C(4096)
#define N24C32_PAGE_SIZE            UINT32_C(32)
    
#define N24C32_I2C_MIN_WRITE_DELAY  UINT8_C(4)



// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct n24c32_descriptor n24c32_t;
typedef struct n24c32_config_descriptor n24c32_config_t;
typedef struct n24c32_config_pins_descriptor n24c32_config_pins_t;

typedef uint8_t (*n24c32_i2c_rw_funcptr_t)(uint16_t client_address, uint8_t *data, uint8_t size);
typedef void (*n24c32_gpio_write_funcptr_t)(uint8_t pin, uint8_t value);
typedef void (*n24c32_delay_ms_funcptr_t)(uint32_t ms);


struct n24c32_config_pins_descriptor{
    uint16_t wp;
};

struct n24c32_config_descriptor{
    n24c32_config_pins_t pins;
    uint32_t max_address;
    uint8_t page_size;
};

struct n24c32_descriptor{
    uint16_t client_address;
    
    n24c32_config_t config;
    
    n24c32_i2c_rw_funcptr_t i2c_write;
    n24c32_i2c_rw_funcptr_t i2c_read;
    
    n24c32_gpio_write_funcptr_t gpio_write;
    
    n24c32_delay_ms_funcptr_t delay_ms;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************

/**
 * \brief Function for initialization n24c32 module.
 *
 * \param me pointer to n24c32 module type of n24c32_t
 * \param config pointer to n24c32 configuration type of n24c32_config_t
 *
 * \returns status of success / failure
 */
uint8_t n24c32_init (n24c32_t *me, uint16_t client_address, n24c32_config_t *config);

uint8_t n24c32_i2c_write_register (n24c32_t *me, n24c32_i2c_rw_funcptr_t i2c_write_funcptr);

uint8_t n24c32_i2c_read_register (n24c32_t *me, n24c32_i2c_rw_funcptr_t i2c_read_funcptr);

uint8_t n24c32_gpio_write_register (n24c32_t *me, n24c32_gpio_write_funcptr_t gpio_write_funcptr);

uint8_t n24c32_delay_ms_register (n24c32_t *me, n24c32_delay_ms_funcptr_t delay_ms_funcptr);

uint8_t n24c32_write_word (n24c32_t *me, uint32_t address, uint8_t word);

uint8_t n24c32_write_page (n24c32_t *me, uint32_t page_address, uint8_t *page);

uint8_t n24c32_write_address (n24c32_t *me, uint32_t address);

uint8_t n24c32_get_word (n24c32_t *me, uint32_t address, uint8_t *word);

uint8_t n24c32_get_page (n24c32_t *me, uint32_t page_address, uint8_t *page);

uint8_t n24c32_i2c_check (n24c32_t *me);

void n24c32_lock (n24c32_t *me);

void n24c32_unlock (n24c32_t *me);


#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of N24C32_H
