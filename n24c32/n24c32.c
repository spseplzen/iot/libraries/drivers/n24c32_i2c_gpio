/*
 * File:   n24c32.c
 * Author: Miroslav Soukup
 * Description: Source file of n24c32 eeprom memory driver.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "n24c32.h"


// *****************************************************************************
// Section: Functions
// *****************************************************************************

uint8_t n24c32_init (n24c32_t *me, uint16_t client_address, n24c32_config_t *config) {
    me->config = *config;
    me->client_address = client_address;
    
    n24c32_unlock(me);
    
    if(!n24c32_i2c_check(me)) return 0;
    
    return 1;
}

uint8_t n24c32_i2c_write_register (n24c32_t *me, n24c32_i2c_rw_funcptr_t i2c_write_funcptr){
    if(!i2c_write_funcptr) return 0;
    me->i2c_write = i2c_write_funcptr;
    return 1;
}

uint8_t n24c32_i2c_read_register (n24c32_t *me, n24c32_i2c_rw_funcptr_t i2c_read_funcptr){
    if(!i2c_read_funcptr) return 0;
    me->i2c_read = i2c_read_funcptr;
    return 1;
}

uint8_t n24c32_gpio_write_register (n24c32_t *me, n24c32_gpio_write_funcptr_t gpio_write_funcptr){
    if(!gpio_write_funcptr) return 0;
    me->gpio_write = gpio_write_funcptr;
    return 1;
}

uint8_t n24c32_delay_ms_register (n24c32_t *me, n24c32_delay_ms_funcptr_t delay_ms_funcptr){
    if(!delay_ms_funcptr) return 0;
    me->delay_ms = delay_ms_funcptr;
    return 1;
}

uint8_t n24c32_write_word (n24c32_t *me, uint32_t address, uint8_t word){
    if(address >= me->config.max_address) return 0;
    static uint8_t _data[3];
    _data[0] = (address >> 8) & 0xFF;
    _data[1] = address & 0xFF;
    _data[2] = word;
    if(!me->i2c_write(me->client_address, _data, 3)) return 0;
    me->delay_ms(N24C32_I2C_MIN_WRITE_DELAY);
    return 1;
}

uint8_t n24c32_write_page (n24c32_t *me, uint32_t page_address, uint8_t *page){
    if(page_address >= (me->config.max_address / me->config.page_size)) return 0;
    
    page_address = page_address * me->config.page_size;
    
    static uint8_t _data[N24C32_PAGE_SIZE + 2];
    _data[0] = (page_address >> 8) & 0xFF;
    _data[1] = page_address & 0xFF;
    
    for(uint8_t i = 0; i < me->config.page_size; ++i)
        _data[i + 2] = page[i];
    
    if(!me->i2c_write(me->client_address, _data, me->config.page_size + 2)) return 0;
    me->delay_ms(N24C32_I2C_MIN_WRITE_DELAY);
    
    return 1;
}

uint8_t n24c32_write_address (n24c32_t *me, uint32_t address){
    if(address >= me->config.max_address) return 0;
    static uint8_t _data[2];
    _data[0] = (address >> 8) & 0xFF;
    _data[1] = address & 0xFF;
    if(!me->i2c_write(me->client_address, _data, 2)) return 0;
    me->delay_ms(N24C32_I2C_MIN_WRITE_DELAY);
    return 1;
}

uint8_t n24c32_get_word (n24c32_t *me, uint32_t address, uint8_t *word){
    if(address >= me->config.max_address) return 0;
    static uint8_t _data[2];
    _data[0] = (address >> 8) & 0xFF;
    _data[1] = address & 0xFF;
    if(!me->i2c_write(me->client_address, _data, 2)) return 0;
    me->delay_ms(N24C32_I2C_MIN_WRITE_DELAY);
    if(!me->i2c_read(me->client_address, word, 1)) return 0;
    return 1;
}

uint8_t n24c32_get_page (n24c32_t *me, uint32_t page_address, uint8_t *page){
    if(page_address >= (me->config.max_address / me->config.page_size)) return 0;
    
    page_address = page_address * me->config.page_size;
    
    static uint8_t _data[N24C32_PAGE_SIZE + 2];
    _data[0] = (page_address >> 8) & 0xFF;
    _data[1] = page_address & 0xFF;
    
    if(!me->i2c_write(me->client_address, _data, 2)) return 0;
    me->delay_ms(N24C32_I2C_MIN_WRITE_DELAY);
    
    if(!me->i2c_read(me->client_address, page, me->config.page_size)) return 0;
    
    return 1;
}

uint8_t n24c32_i2c_check (n24c32_t *me){
    uint8_t _byte;
    return me->i2c_read(me->client_address, &_byte, 1);
}

void n24c32_lock (n24c32_t *me){
    me->gpio_write(me->config.pins.wp, 1);
}

void n24c32_unlock (n24c32_t *me){
    me->gpio_write(me->config.pins.wp, 0);
}
