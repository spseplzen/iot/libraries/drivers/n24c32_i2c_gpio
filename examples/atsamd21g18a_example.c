#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"

#include "n24c32/n24c32.h"


uint8_t i2c_read(uint16_t client_address, uint8_t *data, uint8_t size){
    SERCOM1_I2C_Read(client_address, data, size);
    while(SERCOM1_I2C_IsBusy());
    return !(SERCOM1_I2C_ErrorGet());
}

uint8_t i2c_write(uint16_t client_address, uint8_t *data, uint8_t size){
    SERCOM1_I2C_Write(client_address, data, size);
    while(SERCOM1_I2C_IsBusy());
    return !(SERCOM1_I2C_ErrorGet());
}

void gpio_write(uint8_t pin, uint8_t value){
    PORT_PinWrite((PORT_PIN) pin, (_Bool) value);
}

void delay_ms(uint32_t ms){
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(ms);
    SYSTICK_TimerStop();
}


int main(void)
{
    SYS_Initialize(NULL);
    
    
    n24c32_t myeeprom;
    
    n24c32_i2c_write_register(&myeeprom, i2c_write);
    n24c32_i2c_read_register(&myeeprom, i2c_read);
    n24c32_gpio_write_register(&myeeprom, gpio_write);
    n24c32_delay_ms_register(&myeeprom, delay_ms);
    
    
    n24c32_config_t config;
    config.max_address = N24C32_WORDS_COUNT_MAX;
    config.page_size   = N24C32_PAGE_SIZE;
    config.pins.wp     = mikroBUS_WP_PIN;
    
    n24c32_init(&myeeprom, N24C32_I2C_CLIENT_ADDRESS, &config);

    
    for(uint32_t i = 0; i < N24C32_WORDS_COUNT_MAX; ++i){
        if((i % N24C32_PAGE_SIZE) == 0) printf("EEPROM: %3.02f%%\r", ((float) i / (N24C32_WORDS_COUNT_MAX - 1)) * 100.0);
        if(!n24c32_write_word(&myeeprom, i, 0)) printf("I2C ERROR 1\r\n");
    }
    printf("\n");
    
    uint8_t page[32] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    if(!n24c32_write_page(&myeeprom, 0, page)) printf("I2C ERROR 1\r\n");
    
    if(!n24c32_write_word(&myeeprom, 32, 2)) printf("I2C ERROR 1\r\n");

    
    n24c32_lock(&myeeprom);
    
    
    printf("data: {");
    
    for(uint32_t i = 0; i < 64; ++i){
        
        uint8_t d;
        if(!n24c32_get_word(&myeeprom, i, &d)) printf("I2C ERROR 4 : %lu\r\n", i);
        
        if((i % 32) == 0){
            printf("\r\n        (%5lu - %5lu):  %3u", i, i + 31, d);
        }
        else
            printf(" ,%3u", d);
    }
    
    printf("\r\n       }\r\n");
  
    
    while (true)
    {
        SYS_Tasks();
        
    }

    return ( EXIT_FAILURE );
}
